/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package com.forrest.ipshare;

import com.beust.jcommander.Parameter;

/**
 * @author andrew.forrest@forgerock.com
 */
public enum Globals {

    INSTANCE;

    @Parameter(names = {"--hostname", "-h"}, required = true, description = "The local hostname")
    private String hostName = null;

    @Parameter(names = {"--shared", "-s"}, required = true, description = "The shared IP file")
    private String sharedFile = null;

    @Parameter(names = {"--hostsfile", "-f"}, description = "The hosts file")
    private String hostsFile = "/Users/apforrest/Downloads/hosts";

    public String getSharedFile() {
        return sharedFile;
    }

    public String getHostName() {
        return hostName;
    }

    public String getHostsFile() {
        return hostsFile;
    }

}
