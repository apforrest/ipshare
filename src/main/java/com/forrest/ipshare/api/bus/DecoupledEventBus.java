/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package com.forrest.ipshare.api.bus;

import com.forrest.ipshare.api.ActionListener;
import com.forrest.ipshare.api.Consumer;
import com.forrest.ipshare.api.Event;
import com.forrest.ipshare.api.EventBus;
import com.forrest.ipshare.api.EventType;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

/**
 * @author andrew.forrest@forgerock.com
 */
public class DecoupledEventBus implements EventBus {

    private final WriteLock writeLock;
    private final ReadLock readLock;

    private final Map<EventType, Set<Consumer>> consumerCache;
    private final BlockingQueue<Event> queue;
    private final ExecutorService executor;
    private final EventHandler handler;

    public DecoupledEventBus(int eventLoad) {
        final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
        writeLock = lock.writeLock();
        readLock = lock.readLock();

        consumerCache = new HashMap<EventType, Set<Consumer>>();
        queue = new ArrayBlockingQueue<Event>(eventLoad);

        executor = Executors.newFixedThreadPool(21);

        handler = new EventHandler(queue, new ActionListener<Event>() {

            @Override
            public void notifyAction(Event event) {
                notifyConsumers(event);
            }

        });
        executor.execute(handler);
    }

    @Override
    public void placeEvent(Event event) {
        boolean added = false;
        while (!added) {
            try {
                queue.put(event);
                added = true;
            } catch (InterruptedException iE) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public void registerConsumer(Consumer consumer, EventType... eventTypes) {
        for (EventType eventType : eventTypes) {
            registerConsumer(consumer, eventType);
        }
    }

    private void registerConsumer(Consumer consumer, EventType eventType) {
        if (!consumerCache.containsKey(eventType)) {
            writeLock.lock();
            if (!consumerCache.containsKey(eventType)) {
                consumerCache.put(eventType, new CopyOnWriteArraySet<Consumer>());
            }
            writeLock.unlock();
        }

        // No need to lock here.
        consumerCache.get(eventType).add(consumer);
    }

    @Override
    public void removeConsumer(Consumer consumer) {
        readLock.lock();
        for (Set<Consumer> consumers : consumerCache.values()) {
            consumers.remove(consumer);
        }
        readLock.unlock();
    }

    @Override
    public void removeConsumer(Consumer consumer, EventType... eventTypes) {
        for (EventType eventType : eventTypes) {
            removeConsumer(consumer, eventType);
        }
    }

    private void removeConsumer(Consumer consumer, EventType eventType) {
        readLock.lock();
        final Set<Consumer> consumers = consumerCache.get(eventType);
        readLock.unlock();

        if (consumers == null || consumers.isEmpty()) {
            return;
        }

        consumers.remove(consumer);
    }

    @Override
    public void shutdown() {
        handler.shutdown();
        executor.shutdown();
    }

    private void notifyConsumers(Event event) {
        readLock.lock();
        final Set<Consumer> consumers = consumerCache.get(event.getType());
        readLock.unlock();

        if (consumers == null || consumers.isEmpty()) {
            return;
        }

        executor.execute(new EventDistributor(event, consumers));
    }

    private final static class EventHandler implements Runnable {

        private final BlockingQueue<Event> queue;
        private final ActionListener<Event> listener;
        private boolean shutdown;

        public EventHandler(BlockingQueue<Event> queue, ActionListener<Event> listener) {
            this.queue = queue;
            this.listener = listener;
        }

        @Override
        public void run() {
            while (!shutdown) {
                final Event event = pull();

                if (event == null) {
                    continue;
                }

                listener.notifyAction(event);
            }
        }

        private Event pull() {
            Event event = null;

            try {
                event = queue.take();
            } catch (InterruptedException iE) {
                Thread.currentThread().interrupt();
            }

            return event;
        }

        public void shutdown() {
            shutdown = true;
        }

    }

    private final static class EventDistributor implements Runnable {

        private final Event event;
        private final Set<Consumer> consumers;

        public EventDistributor(Event event, Set<Consumer> consumers) {
            this.event = event;
            this.consumers = consumers;
        }

        @Override
        public void run() {
            for (Consumer consumer : consumers) {
                consumer.consume(event);
            }
        }

    }

}

