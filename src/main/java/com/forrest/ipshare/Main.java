/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package com.forrest.ipshare;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.forrest.ipshare.api.Consumer;
import com.forrest.ipshare.api.EventBus;
import com.forrest.ipshare.api.bus.DecoupledEventBus;
import com.forrest.ipshare.consumers.LocalIpChangeHandler;
import com.forrest.ipshare.consumers.RemoteIpChangeHandler;
import com.forrest.ipshare.producers.LocalIpChange;
import com.forrest.ipshare.producers.SharedFileChange;
import com.forrest.ipshare.utils.FileHandler;
import com.forrest.ipshare.utils.IpEventTypes;
import com.forrest.ipshare.utils.SimpleFileHandler;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * @author andrew.forrest@forgerock.com
 */
public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        BasicConfigurator.configure();

        Globals globals = Globals.INSTANCE;
        JCommander commander = new JCommander(globals);
        commander.setProgramName("IP Share");

        try {
            commander.parse(args);
            new Main().start(globals);
        } catch (ParameterException pE) {
            System.out.println(pE.getMessage());
            commander.usage();
        }
    }

    public void start(Globals globals) {
        LOGGER.debug("Startup");
        EventBus bus = new DecoupledEventBus(100);

        FileHandler sharedHandler = new SimpleFileHandler(globals.getSharedFile());
        FileHandler hostsHandler = new SimpleFileHandler(globals.getHostsFile());
        Consumer localIpChangeHandler = new LocalIpChangeHandler(sharedHandler);
        Consumer remoteIpChangeHandler = new RemoteIpChangeHandler(hostsHandler);

        bus.registerConsumer(localIpChangeHandler, IpEventTypes.LOCAL_IP_CHANGE);
        bus.registerConsumer(remoteIpChangeHandler, IpEventTypes.REMOTE_IP_CHANGE);

        new LocalIpChange(bus);
        new SharedFileChange(bus, sharedHandler);
    }

}
