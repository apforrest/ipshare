/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package com.forrest.ipshare.consumers;

import com.forrest.ipshare.utils.FileHandler;
import com.forrest.ipshare.api.Consumer;
import com.forrest.ipshare.api.Event;
import com.forrest.ipshare.utils.Address;
import com.forrest.ipshare.utils.IpEvent;
import com.forrest.ipshare.utils.IpEventTypes;
import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.List;

/**
 * @author andrew.forrest@forgerock.com
 */
public class LocalIpChangeHandler implements Consumer {

    private static final Logger LOGGER = Logger.getLogger(LocalIpChangeHandler.class);

    private final FileHandler handler;

    public LocalIpChangeHandler(FileHandler handler) {
        this.handler = handler;
    }

    @Override
    public <E extends Event> void consume(E event) {
        if (IpEventTypes.LOCAL_IP_CHANGE == event.getType()) {
            handleLocalIpEvent(IpEventTypes.LOCAL_IP_CHANGE.cast(event));
        }
    }

    private void handleLocalIpEvent(IpEvent event) {
        final List<String> hosts = handler.read();
        final Iterator<String> iterator = hosts.iterator();
        final Address address = event.getAddress();

        while (iterator.hasNext()) {
            final String line = iterator.next();

            if (line.contains(address.getHostname())) {
                // Host entry already exists, remove it.
                iterator.remove();
            }
        }

        LOGGER.debug(String.format("New entry added to the shared file %s.", address));
        hosts.add(address.getIp() + ' ' + address.getHostname());
        handler.write(hosts);
    }

}
