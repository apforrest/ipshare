/*
 * Copyright 2014 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forrest.ipshare.producers;

import com.forrest.ipshare.utils.FileHandler;
import com.forrest.ipshare.api.ActionListener;
import com.forrest.ipshare.utils.Address;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 *
 */
public class FileMonitor implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(FileMonitor.class);

    private static final Pattern WHITE_PATTERN = Pattern.compile("\\s+");
    private static final Pattern IP_PATTERN = Pattern.compile("^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$");
    private static final Pattern HOSTNAME_PATTERN = Pattern.compile("^[0-9a-zA-Z_\\-\\.]+$");

    private final FileHandler handler;
    private final ActionListener<Address> listener;
    private final Map<String, Address> cache;

    public FileMonitor(FileHandler handler, ActionListener<Address> listener) {
        this.handler = handler;
        this.listener = listener;
        cache = new HashMap<String, Address>();
    }

    @Override
    public void run() {
        LOGGER.debug("Checking shared file.");
        final List<String> lines = handler.read();

        for (String line : lines) {
            final Address address = parseLine(line);
            final Address previousAddress = cache.get(address.getHostname());

            if (previousAddress == null || !address.equals(previousAddress)) {
                cache.put(address.getHostname(), address);
                listener.notifyAction(address);
            }
        }

    }

    private Address parseLine(String line) {
        String[] values = WHITE_PATTERN.split(line);

        if (values.length != 2) {
            throw new IllegalArgumentException("File should be ip/hostname pairs");
        }

        String ipAddress = values[0];
        if (!IP_PATTERN.matcher(ipAddress).matches()) {
            throw new IllegalArgumentException("Invalid IPv4 address " + ipAddress);
        }

        String hostName = values[1];
        if (!HOSTNAME_PATTERN.matcher(hostName).matches()) {
            throw new IllegalArgumentException("Invalid hostname " + hostName);
        }

        return new Address(ipAddress, hostName);
    }

}
