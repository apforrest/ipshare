/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package com.forrest.ipshare.producers;

import com.forrest.ipshare.utils.FileHandler;
import com.forrest.ipshare.api.ActionListener;
import com.forrest.ipshare.api.EventBus;
import com.forrest.ipshare.api.Producer;
import com.forrest.ipshare.utils.Address;
import com.forrest.ipshare.utils.IpEventTypes;
import org.apache.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Watches for new entries in the shared file and creates event when new entries appear.
 *
 * @author andrew.forrest@forgerock.com
 */
public class SharedFileChange implements Producer {

    private static final Logger LOGGER = Logger.getLogger(SharedFileChange.class);

    private final EventBus bus;

    public SharedFileChange(EventBus bus, FileHandler handler) {
        this.bus = bus;

        final FileMonitor monitor = new FileMonitor(handler, new ActionListener<Address>() {

            @Override
            public void notifyAction(Address event) {
                shareEvent(event);
            }

        });

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleWithFixedDelay(monitor, 0L, 5L, TimeUnit.SECONDS);
    }

    private void shareEvent(Address newAddress) {
        LOGGER.debug(String.format("New remote IP present %s.", newAddress));
        bus.placeEvent(IpEventTypes.createRemoteEvent(newAddress));
    }

}
