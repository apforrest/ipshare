/*
 * Copyright 2014 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forrest.ipshare.producers;

import com.forrest.ipshare.api.ActionListener;
import com.forrest.ipshare.utils.NetworkUtils;
import org.apache.log4j.Logger;

import java.net.InetAddress;

/**
 *
 */
public class LocalIpMonitor implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(LocalIpChange.class);

    private final ActionListener<String> ipListener;
    private String lastCapturedIp;

    public LocalIpMonitor(ActionListener<String> ipListener) {
        this.ipListener = ipListener;
    }

    @Override
    public void run() {
        LOGGER.debug(String.format("Checking local IP."));
        InetAddress address = NetworkUtils.getLocalAddress();

        if (address == null || address.getHostAddress() == null) {
            return;
        }

        String currentIp = address.getHostAddress();

        if (!currentIp.equals(lastCapturedIp)) {
            lastCapturedIp = currentIp;
            ipListener.notifyAction(currentIp);
        }
    }

}
