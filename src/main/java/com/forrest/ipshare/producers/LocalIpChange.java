/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package com.forrest.ipshare.producers;

import com.forrest.ipshare.Globals;
import com.forrest.ipshare.api.ActionListener;
import com.forrest.ipshare.api.EventBus;
import com.forrest.ipshare.api.Producer;
import com.forrest.ipshare.utils.Address;
import com.forrest.ipshare.utils.IpEventTypes;
import com.forrest.ipshare.utils.NetworkUtils;
import org.apache.log4j.Logger;

import java.net.InetAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Watches for a change in the machines IP and creates an event when it does.
 *
 * @author andrew.forrest@forgerock.com
 */
public class LocalIpChange implements Producer {

    private static final Logger LOGGER = Logger.getLogger(LocalIpChange.class);

    private final EventBus bus;

    public LocalIpChange(EventBus bus) {
        this.bus = bus;

        LocalIpMonitor monitorLocalIp = new LocalIpMonitor(new ActionListener<String>() {

            @Override
            public void notifyAction(String newIp) {
                shareEvent(newIp);
            }

        });

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleWithFixedDelay(monitorLocalIp, 0L, 5L, TimeUnit.SECONDS);
    }

    private void shareEvent(String newIp) {
        Address address = new Address(newIp, Globals.INSTANCE.getHostName());
        LOGGER.debug(String.format("Local IP has changed %s.", address));
        bus.placeEvent(IpEventTypes.createLocalEvent(address));
    }

}
