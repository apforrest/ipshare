/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package com.forrest.ipshare.utils;

/**
 * @author andrew.forrest@forgerock.com
 */
public class Address {

    private final String ip;
    private final String hostname;

    public Address(String ip, String hostname) {
        if (ip == null) {
            throw new NullPointerException("IP can not be null");
        }

        if (hostname == null) {
            throw new NullPointerException("Hostname can not be null");
        }

        this.ip = ip;
        this.hostname = hostname;
    }

    public String getIp() {
        return ip;
    }

    public String getHostname() {
        return hostname;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Address)) {
            return false;
        }

        Address address = (Address)o;

        if (!ip.equals(address.ip)) {
            return false;
        }

        if (!hostname.equals(address.hostname)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Address[ip=" + ip + ",hostname=" + hostname + "]";
    }
}
