/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package com.forrest.ipshare.utils;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author andrew.forrest@forgerock.com
 */
public class SimpleFileHandler implements FileHandler {

    private final static Logger LOGGER = Logger.getLogger(SimpleFileHandler.class);

    private final File file;
    private long timestampAtRead;

    public SimpleFileHandler(String filename) {
        this(filename, false);
    }

    public SimpleFileHandler(String filename, boolean createIfMissing) {
        if (filename == null) {
            throw new NullPointerException("Filename should not be null");
        }

        if (filename.isEmpty()) {
            throw new IllegalArgumentException("A valid filename should be passed");
        }

        file = new File(filename);

        if (!file.exists()) {
            handleMissingFile(createIfMissing);
        }

        timestampAtRead = Long.MAX_VALUE;
    }

    private void handleMissingFile(boolean createIfMissing) {
        if (!createIfMissing) {
            throw new IllegalStateException("File does not exist");
        }

        try {
            file.createNewFile();
        } catch (IOException ioE) {
            throw new IllegalStateException("Unable to create file", ioE);
        }
    }

    /**
     * {@inheritDoc}
     */
    public List<String> read() {
        BufferedReader reader = null;

        try {
            timestampAtRead = file.lastModified();

            reader = new BufferedReader(new FileReader(file));
            List<String> lines = new ArrayList<String>();

            // Read first line.
            String line = reader.readLine();

            while (line != null) {
                lines.add(line);
                // Read consecutive lines.
                line = reader.readLine();
            }

            return lines;
        } catch (FileNotFoundException fnfE) {
            throw new IllegalStateException("Failed reading the file", fnfE);
        } catch (IOException ioE) {
            throw new IllegalStateException("Failed reading the file", ioE);
        } finally {
            close(reader);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void write(List<String> lines) {
        if (isStaleSinceLastRead()) {
            LOGGER.warn("Writing stale data");
        }

        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file));

            for (String line : lines) {
                writer.write(line + '\n');
            }
        } catch (IOException ioE) {
            throw new IllegalStateException("Failed to save the file", ioE);
        } finally {
            close(writer);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isStaleSinceLastRead() {
        return timestampAtRead < file.lastModified();
    }

    /**
     * Attempt to close the object.
     *
     * @param closeable
     *         Object to be closed.
     */
    private void close(Closeable closeable) {
        if (closeable == null) {
            return;
        }

        try {
            closeable.close();
        } catch (IOException ioE) {
            // TODO: Log that closeable couldn't be closed.
        }
    }

}
