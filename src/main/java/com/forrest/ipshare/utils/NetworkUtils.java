/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2013 ForgeRock Inc.
 */
package com.forrest.ipshare.utils;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * @author andrew.forrest@forgerock.com
 */
public final class NetworkUtils {

    private NetworkUtils() {
        throw new Error("This class should not be instantiated");
    }

    /**
     * Attempts to identify the valid local internet address.
     *
     * @return A valid internet address.
     */
    public static InetAddress getLocalAddress() {
        try {
            // Retrieve all network interfaces.
            return searchNetworkInterfaces(NetworkInterface.getNetworkInterfaces());
        } catch (SocketException sE) {
            throw new IllegalStateException("Unable to retrieve the local network address", sE);
        }
    }

    private static InetAddress searchNetworkInterfaces(Enumeration<NetworkInterface> nInterfaces) throws SocketException {
        while (nInterfaces.hasMoreElements()) {
            final NetworkInterface nInterface = nInterfaces.nextElement();

            if (!nInterface.isUp() || nInterface.isLoopback() || nInterface.isVirtual()) {
                continue;
            }

            // Retrieve all possible addresses for the valid interface.
            return analysisAddresses(nInterface.getInetAddresses());
        }

        return null;
    }

    private static InetAddress analysisAddresses(Enumeration<InetAddress> addresses) {
        while (addresses.hasMoreElements()) {
            final InetAddress address = addresses.nextElement();

            if (address.isLoopbackAddress()) {
                continue;
            }

            if (address instanceof Inet4Address) {
                // Valid IPv4 address found.
                return address;
            }
        }

        return null;
    }

}
